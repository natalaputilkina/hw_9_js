/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

- це структура документа у вигляді дерева

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerText виводить лише текст без HTML-форматування, innerHTML дозволяє маніпулювати HTML-структурою елементів, включаючи теги та стилі. 

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

до елементів верхнього рівня:
- document.documentElement - для звернення до елементу
- document.head - для звернення до елементу
- document.body - для звернення до елементу

до будь-якого елементу сторінки:

- getElementById () - пошук елемента за id
- getElementsByName () - пошук по всіх елементах сторінки із заданим атрибутом name
- getElementsByTagName () - пошук по тегу
- getElementsByClassName () - пошук по глобальному атрибуту class
- querySelectorAll () - повертає список всіх елементів, що задовольняють заданій CSS-селектору, при його виклику пошук ведеться всередині даного елемента
- querySelector () - повертає перший елемент, що задовольняє заданому CSS-селектору, при його виклику пошук ведеться всередині даного елемента

4. Яка різниця між nodeList та HTMLCollection?

1. NodeList:
- це колекція вузлів, яка повертається різними методами
DOM, такими як `querySelectorAll`.
- це загальна колекція, яка може містити будь-який тип вузла
(елементи, текстові вузли і т.д.).
- за замовчуванням не має деяких методів масиву, таких як
`forEach`, `map` і т.д. Щоб використовувати ці методи, його слід
конвертувати в масив.

2. HTMLCollection:
- Вона повертається конкретними методами DOM, такими як
`getElementsByTagName` чи `getElementsByClassName`.
- Це жива колекція, тобто вона автоматично оновлюється при зміні
документа.
- трохи більш обмежена, ніж `NodeList`, оскільки
вона містить тільки елементи.
- є деякі додаткові властивості та методи, специфічні для
HTML, такі як `namedItem` для отримання елемента за іменем.

NodeList є більш загальною і може включати будь-який тип вузла, в той
час як `HTMLCollection` специфічно стосується HTML-елементів.

Практичні завдання

 */

// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів. 
// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

// 1 спосіб:

// let features = document.getElementsByClassName("feature");


// [...features].forEach((element) => {
//     console.log(element);
//     element.style.textAlign = 'center';
    
// });


// 2 спосіб:
// const features = document.querySelectorAll('.feature')

// features.forEach((element) => {
//     console.log(element);
//     element.style.textAlign = 'center';
    
// });

// 2. Змініть текст усіх елементів h2 на "Awesome feature".

// const elementH2 = document.querySelectorAll('h2');

// const a = (item) => {

//     item.textContent = 'Awesome feature';

// }

// elementH2.forEach(a) 


// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 
// const featureTitle = document.querySelectorAll('.feature-title')
// console.log(featureTitle)

// featureTitle.forEach((element) =>{
//     element.innerHTML += '!'
// })